package com.csvfilereader.pojo;

public class Position {
private int xValue;
private int yValue;
private int deltaSquare;
public int getxValue() {
	return xValue;
}
public void setxValue(int xValue) {
	this.xValue = xValue;
}
public int getyValue() {
	return yValue;
}
public void setyValue(int yValue) {
	this.yValue = yValue;
}
public int getDeltaSquare() {
	return deltaSquare;
}
public void setDeltaSquare(int deltaSquare) {
	this.deltaSquare = deltaSquare;
}

}
