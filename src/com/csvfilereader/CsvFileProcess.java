package com.csvfilereader;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.List;

import com.csvfilereader.pojo.Position;
import com.opencsv.bean.CsvToBeanBuilder;
public class CsvFileProcess {
	 
	
	private static final int DeltaSquare = 0;

	public static void main(String[] args) {

        String fileName = "c:\\csv\\position.csv";
        String equation="11+15x";
        int sizeFile=0;
        int bb=-100;

        try {
			List<Position> position = new CsvToBeanBuilder(new FileReader(fileName))
			        .withType(Position.class)
			        .build()
			        .parse();
			
			sizeFile=position.size();
			System.out.println("File Size  "+sizeFile);
			int DeltaSquare=0;
			for(Position i:position) {
				
				String removedChar=equation.substring(0, equation.length()-1).trim();
				System.out.println("removedChar  "+removedChar);
				String[] strarr=removedChar.split("\\+",2);
				System.out.println(strarr);
				String xvalue="";
				String yvalue="";	
				
				for(int j=0;j<=strarr.length;j++) {
					
					if(j==0)
					{
						xvalue=strarr[0];	
					}
					else
					{
						yvalue=strarr[1];	
					}
				}
				
				System.out.println("xvalue  "+xvalue);
				System.out.println("yvalue  "+yvalue);
				int y=Integer.parseInt(xvalue)+(Integer.parseInt(xvalue) * i.getyValue());
				int y_value=y-i.getyValue();
				i.setDeltaSquare(y_value*y_value);
				System.out.println(y_value);
				DeltaSquare+=i.getDeltaSquare();
				
			}
			System.out.println("DeltaSquare "+DeltaSquare);
			System.out.println("Final output "+DeltaSquare/sizeFile);
		} catch (IllegalStateException | FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
